## CS242 – Fall 2020

# Lab 2 – Linked Lists in Java

# Objective

The objective of this Lab is to understand and apply the LinkedList Abstract Data Type container available with the Java Collections Framework.  

### Prerequisite

Open the project in **IntelliJ IDE**. Also, Labs have to be done collaboratively as a team by using **CodeTogether PlugIn**. The team member designated as Spokesperson for the week is responsible for downloading the project and hosting the project through "Code Together" with rest of the team members. Build the project. Make sure your project compiles without errors.

### Team Roles

Please find below description of Team Roles:

| **Role**                 | **Team Member** | **Responsibilities**                                                                                                                 |
| ------------------------ | --------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| Facilitator              |                 | Reads the questions aloud, keeps track of time and makes sure everyone contributes appropriately.                                    |
| Spokesperson / Presenter |                 | Talks to the instructor and other teams. Compiles and runs programs when applicable. Presents the team’s answers.                    |
| Recorder                 |                 | Makes a copy of POGIL activity document. Records team’s answers to all questions. Records important aspects of team discussions.     |
| Reflector                |                 | Provides team reflection to team & instructor. Considers how the team could work and learn more effectively. Ensures team consensus. |

### Project Description

The skeleton of the Project has been provided. Make sure the project builds without errors, before you start making any changes.

**Before you start making the changes, discuss the Project structure and inheritance amongst team members.**

All changes/additions have to be made in Main.java

Declare 4 objects of PerishableItem  

Declare 4 objects of MiscellaneousItem 

Add them to the GroceryList using different methods given in the table of POGIL Activity3.  

Also, make sure that set and remove methods are used,  such that after using each of the 8 objects created at least once,  at the end of all statements, the size of GroceryList is 6.

Also, complete the enhanced for loop.

### Deliverable

In order to submit your program, you will have to:

1. Spokesperson forks Lab2 from [https://gitlab.com/worcester/cs/cs-242-01-02-03-fall-2020/cs-242-lab2](https://gitlab.com/worcester/cs/cs-242-01-02-03-fall-2020/cs-242-lab2) to his/her namespace.

2. Clone it to your local repository.

3. Spokesperson compiles and hosts/shares CodeTogether URL with all team members.

4. After the project is complete, Spokesperson Commits and Pushes final repository to GitLab.

5. Facilitator makes sure that all team member names have been added as Author Names and the Instructor (snagpal) has been given the *Maintainer / Developer* access.

**Due Date**

As given on Blackboard.

## Copyright and License

#### &copy; 2020 S Nagpal, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
