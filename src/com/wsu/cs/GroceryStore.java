package com.wsu.cs;
/**
 * Represents a GroceryStore with itemName and itemQuantity
 *
 * @author S Nagpal
 * version 2020
 */
public class GroceryStore {
    private String itemName;
    private int itemQuantity;

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    @Override
    public String toString() {
        return itemName + " : Quantity " + itemQuantity;
    }
}
