package com.wsu.cs;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Represents the test class GroceryStoreTest
 * Implemented with JUnit 5 - Jupiter Tests
 *
 * @author S Nagpal
 * version 2020
 */

class GroceryStoreTest {

    private static PerishableItem pi1, pi2;
    private static MiscellaneousItem mi3, mi4;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        pi1 = new PerishableItem("Milk", 2, 9, 30, 2020);
        pi2 = new PerishableItem("Eggs", 2, 10, 10, 2020);

        mi3 = new MiscellaneousItem("Laundary Detergent", 1,2020);
        mi4 = new MiscellaneousItem("Shampoo", 1,2019);

    }

    @org.junit.jupiter.api.Test
    public void test_PerishableItem() {
        assertEquals("Milk : Quantity 2 expires on 9-30-2020", pi1.toString());
        assertEquals("Eggs : Quantity 2 expires on 10-10-2020", pi2.toString());
    }

    @org.junit.jupiter.api.Test
    public void test_MiscellaneousItem() {
        assertEquals("Laundary Detergent : Quantity 1 was manufactured in the year 2020", mi3.toString());
        assertEquals("Shampoo : Quantity 1 was manufactured in the year 2019", mi4.toString());
    }

}