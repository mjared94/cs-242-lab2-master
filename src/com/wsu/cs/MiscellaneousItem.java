package com.wsu.cs;

/**
 * Represents a MiscellaneousItem  extends GroceryStore
 * with manufactureYear
 *
 * @author S Nagpal
 * version 2020
 */

public class MiscellaneousItem extends GroceryStore {
    private int manufactureYear;

    MiscellaneousItem(String itemName, int itemQuantity, int year) {
        setItemName(itemName);
        setItemQuantity(itemQuantity);
        this.manufactureYear = year;
    }

    public void setManufactureYear(int manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    @Override
    public String toString() {
        return super.toString() + " was manufactured in the year "+ this.manufactureYear;
    }
}
