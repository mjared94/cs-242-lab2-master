package com.wsu.cs;

/**
 * Represents a PerishableItem  extends GroceryStore
 * with expirationMonth, expirationDay, expirationYear
 *
 * @author S Nagpal
 * version 2020
 */

public class PerishableItem extends GroceryStore {
    private int expirationMonth;
    private int expirationDay;
    private int expirationYear;

    PerishableItem(String itemName, int itemQuantity, int month, int day, int year) {
        setItemName(itemName);
        setItemQuantity(itemQuantity);
        this.expirationMonth = month;
        this.expirationDay = day;
        this.expirationYear = year;
    }

    public void setExpirationDate(int month, int day, int year) {
        this.expirationMonth = month;
        this.expirationDay = day;
        this.expirationYear = year;
    }


    @Override
    public String toString() {
        return super.toString() + " expires on " + this.expirationMonth + '-' + this.expirationDay + '-' +this.expirationYear;
    }
}
